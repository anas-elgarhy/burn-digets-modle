use burn::{
    autodiff::ADBackendDecorator,
    backend::{
        wgpu::{AutoGraphicsApi, WgpuDevice},
        WgpuBackend,
    },
    optim::AdamConfig,
};

use crate::modle::ModelConfig;

mod data;
mod modle;

fn main() {
    type TheBackend = WgpuBackend<AutoGraphicsApi, f32, i32>;
    type MyAutodiffBackend = ADBackendDecorator<TheBackend>;

    let device = WgpuDevice::default();

    modle::train::<MyAutodiffBackend>(
        "/tmp/artifacts",
        modle::TrainingConfig::new(ModelConfig::new(10, 512), AdamConfig::new()),
        device,
    );
}
